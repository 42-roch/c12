/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_create_elem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/21 16:32:01 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/22 18:38:58 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_create_elem(void *data)
{
	t_list	*obj;

	obj = NULL;
	obj = malloc(sizeof(t_list));
	if (!obj)
		return (NULL);
	obj->data = data;
	obj->next = NULL;
	return (obj);
}
