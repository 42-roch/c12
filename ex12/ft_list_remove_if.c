/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_remove_if.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/23 16:33:31 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/23 16:41:29 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_remove_if(t_list **begin_list, void *data_ref, int (*cmp)(),
	void (*free_fct)(void *))
{
	t_list	*tmp;
	t_list	*last;

	tmp = *begin_list;
	while (tmp != NULL)
	{
		if (cmp(tmp->data, data_ref) == 0)
		{
			last->next = tmp->next;
			free_fct(tmp->data);
			free(tmp);
		}
		last = tmp;
		tmp = tmp->next;
	}
}
