/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/21 16:29:11 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/22 18:37:11 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LIST_H
# define FT_LIST_H

/**
 * Libraries
 **/
# include <stdio.h>
# include <stdlib.h>

/**
 * Structures
 **/
typedef struct s_list
{
	struct s_list	*next;
	void			*data;
}				t_list;

/**
 * Prototypes
 **/
t_list	*ft_create_elem(void *data);

#endif