/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_sort.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/23 17:16:20 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/23 17:26:38 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

void	ft_list_sort(t_list **begin_list, int (*cmp)())
{
	void	*tempo;
	t_list	*parcour;

	parcour = *begin_list;
	while ((*begin_list)->next)
	{
		if (cmp((*begin_list)->data, (*begin_list)->next->data) > 0)
		{
			tempo = (*begin_list)->data;
			(*begin_list)->data = (*begin_list)->next->data;
			(*begin_list)->next->data = tempo;
			*begin_list = parcour;
		}
		else
			*begin_list = (*begin_list)->next;
	}
	*begin_list = parcour;
}
