/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_push_strs.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rblondia <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/22 17:12:59 by rblondia          #+#    #+#             */
/*   Updated: 2021/09/23 10:58:01 by rblondia         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_list.h"

t_list	*ft_create_obj_(char *data, t_list *next)
{
	t_list	*l;

	l = ft_create_elem(data);
	l->next = next;
	return (l);
}

t_list	*ft_list_push_strs(int size, char **strs)
{
	int		i;
	t_list	*tmp;

	tmp = NULL;
	i = size - 1;
	tmp = ft_create_obj_(strs[0], tmp);
	while (i > 0)
	{
		tmp = ft_create_obj_(strs[i], tmp);
		i--;
	}
	return (tmp);
}
